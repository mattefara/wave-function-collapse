from dataclasses import dataclass
from enum import Enum

import pygame
from pygame import Surface


class Direction(int, Enum):
    TOP = 0
    RIGHT = 1
    BOTTOM = 2
    LEFT = 3

    def invert(self):
        if self == Direction.TOP:
            return Direction.BOTTOM
        if self == Direction.BOTTOM:
            return Direction.TOP
        if self == Direction.LEFT:
            return Direction.RIGHT
        if self == Direction.RIGHT:
            return Direction.LEFT
        raise NotImplementedError()


@dataclass
class Tile:
    image: Surface
    edges: dict[Direction, str]
    position: tuple = (-1, -1)

    def rotate_clockwise(self, amount: int = 1):
        amount = amount % 4

        top, right, bottom, left = self.edges.values()
        for _ in range(amount):
            new_top = left[::-1]
            new_right = top
            new_bottom = right[::-1]
            new_left = bottom
            top, right, bottom, left = new_top, new_right, new_bottom, new_left
        edge, position = list(self.edges.keys()), list(self.edges.values())

        rotated = pygame.transform.rotate(self.image, -90 * amount)
        return Tile(image=rotated, edges={key: value for key, value in zip(edge, [top, right, bottom, left])})

    def rotations(self):
        if self.edges[Direction.TOP] == self.edges[Direction.BOTTOM] and self.edges[Direction.LEFT] == self.edges[Direction.RIGHT] and self.edges[Direction.LEFT] == self.edges[Direction.BOTTOM]:
            return [self.rotate_clockwise(0)]
        return [self.rotate_clockwise(i) for i in range(len(self.edges))]

    def compatible_pieces(self, direction: Direction, tiles: list['Tile']):
        rotations = tiles

        chosen_direction = self.edges[direction]

        compatible_pieces = []
        for i, rotation in enumerate(rotations):
            if rotation.edges[direction.invert()] == chosen_direction:
                compatible_pieces.append(rotation)
        return compatible_pieces
