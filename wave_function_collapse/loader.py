import json
import pygame

from pathlib import Path
from .tile import Tile, Direction


def load_demo(name: str):
    filename = Path('demo') / name / 'description.json'
    with open(filename) as file:
        return json.load(file)


def parse_to_tiles(data: list, base_dir: str, size: int):
    directory = Path(base_dir)
    return [
        Tile(
            pygame.transform.scale(pygame.image.load(directory / tile['image']), (size, size)),
            edges={
                Direction.TOP: tile["edges"]["TOP"],
                Direction.RIGHT: tile["edges"]["RIGHT"],
                Direction.BOTTOM: tile["edges"]["BOTTOM"],
                Direction.LEFT: tile["edges"]["LEFT"]
            }
        ) for tile in data
    ]
