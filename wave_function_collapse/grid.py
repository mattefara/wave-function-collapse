from dataclasses import dataclass
from typing import Union

from .tile import Tile, Direction


@dataclass
class Grid:
    rows: int
    cols: int
    tiles: list[Tile]
    _grid: list[list[Union[Tile, bool]]] = None
    _entropy: list[list[int]] = None
    _entropy_tiles: dict[tuple[int, int], list[Tile]] = None

    def add_tile(self, tile: Tile, position: tuple[int, int], entropy: int = 0):
        x, y = position
        tile.position = position
        self.grid[x][y] = tile
        self.entropy[x][y] = entropy

    @property
    def grid(self):
        if not self._grid:
            self._grid = [[False for _ in range(self.cols)] for _ in range(self.rows)]
            self._entropy_tiles = {}
        return self._grid

    @property
    def entropy(self):
        if not self._entropy:
            self._entropy = [[-1 for y in range(self.cols)] for x in range(self.rows)]
        return self._entropy

    def calculate_entropy(self):
        rotations = []
        for tile in self.tiles:
            rotations.extend(tile.rotations())
        max_rotations = len(rotations)

        for position, tile in self:
            x, y = position
            if tile:
                self.add_tile(tile, position)
                continue

            positions = self.cell_neighbours_positions(position)
            for direction, (neighbour_x, neighbour_y) in positions:
                neighbour_tile = self._grid[neighbour_x][neighbour_y]
                if not neighbour_tile:
                    continue
                possibilities = neighbour_tile.compatible_pieces(direction.invert(), tiles=self._entropy_tiles.get(position, rotations))
                if not possibilities:
                    return None
                self.entropy[x][y] = len(possibilities)
                self._entropy_tiles[position] = possibilities

            if self.entropy[x][y] < 0:
                self.entropy[x][y] = max_rotations
                self._entropy_tiles[position] = rotations

        return self.entropy

    def get_tile_from_entropy(self, position: tuple[int, int]):
        return self._entropy_tiles[position]

    def __iter__(self):
        for i, row in enumerate(self.grid):
            for j, tile in enumerate(row):
                yield (i, j), tile

    def cell_neighbours_positions(self, position: tuple[int, int]):
        x, y = position
        if not (0 <= x < self.cols and 0 <= y < self.rows):
            raise Exception(f'Position outside border: {position}')
        directions = []
        if x - 1 >= 0:
            directions.append((Direction.LEFT, (x - 1, y)))
        if x + 1 < self.cols:
            directions.append((Direction.RIGHT, (x + 1, y)))
        if y - 1 >= 0:
            directions.append((Direction.TOP, (x, y - 1)))
        if y + 1 < self.rows:
            directions.append((Direction.BOTTOM, (x, y + 1)))
        return directions
