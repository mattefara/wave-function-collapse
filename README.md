# Wave function collapse

A simple implementation of `Wave function collapse` using python and pygame.

It's not recommended to create a grid with size greater than 30X30 because the implementation does not use backtracking and the generation restarts every time it cannot place a new tile.

## Simple
First example
![Simple](screenshots/simple.png)

## Circuit
Second example
![Circuit](screenshots/circuit.png)
