import random

import pygame

from wave_function_collapse.grid import Grid
from wave_function_collapse.loader import load_demo, parse_to_tiles
from wave_function_collapse.tile import Tile

WIDTH, HEIGHT = 900, 900
COLUMNS, ROWS = 20, 20
SIZE = WIDTH // COLUMNS


def get_circuit_tiles():
    data = load_demo('circuit')
    return parse_to_tiles(data, 'demo/circuit', SIZE)


def get_simple_tiles():
    data = load_demo('simple')
    return parse_to_tiles(data, 'demo/simple', SIZE)


def get_example(name):
    if name == 'circuit':
        return get_circuit_tiles()
    if name == 'simple':
        return get_simple_tiles()

    raise Exception(f'Cannot find example {name}')


def handle_events(**kwargs):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            kwargs['running'] = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_p:
                kwargs['pause'] = not kwargs.get('pause', False)
    return kwargs


def init_grid(tiles: list[Tile]):
    grid = Grid(cols=COLUMNS, rows=ROWS, tiles=tiles)
    position = random.randint(0, COLUMNS - 1), random.randint(0, ROWS - 1)
    tile = tiles[random.randint(0, len(tiles) - 1)]
    grid.add_tile(tile, position)
    return grid


def main():
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))

    tiles = get_example('circuit')

    grid = init_grid(tiles)

    running = True
    pause = False
    while running:
        events = handle_events(**{'running': running, 'pause': pause})
        pause, running = events.get('pause'), events.get('running')

        if pause:
            pygame.time.wait(500)
            continue

        entropy = grid.calculate_entropy()
        if not entropy:
            grid = init_grid(tiles)
            entropy = grid.calculate_entropy()

        entropy_dict = {}
        for x, col in enumerate(entropy):
            for y, element in enumerate(col):
                entropy_dict[element] = entropy_dict.get(element, []) + [(x, y)]
        sorted_entropy = sorted(entropy_dict)

        if len(sorted_entropy) == 1:
            pygame.time.wait(1000)
            continue

        chosen_position = random.choice(entropy_dict[sorted_entropy[1]])

        for position, tile in grid:
            i, j = position
            xy = (i * SIZE, j * SIZE)
            if position == chosen_position:
                possible_tiles = grid.get_tile_from_entropy(position)
                if not possible_tiles:
                    raise Exception()
                chosen_tile = random.choice(possible_tiles)
                grid.add_tile(chosen_tile, position)
                screen.blit(chosen_tile.image, xy)

            if tile:
                screen.blit(tile.image, xy)

        pygame.display.flip()


if __name__ == '__main__':
    main()
